package br.unicamp.ic.mo631.robodsl.examples;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import br.unicamp.ic.mo631.robodsl.Element;
import br.unicamp.ic.mo631.robodsl.RobodslFactory;
import br.unicamp.ic.mo631.robodsl.RobodslPackage;
import br.unicamp.ic.mo631.robodsl.Robot;
import br.unicamp.ic.mo631.robodsl.RunAway;
import br.unicamp.ic.mo631.robodsl.Scene;
import br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl;

/**
 * This class is an example of how the Ecore APIs of a given metamodel
 * (generated metamodel implementation) can be used to manipulate models.
 * 
 * This example is for the STANDALONE MODE, that is, a normal Java application.
 */

public class StandaloneExample {

	public static void main(String[] args) {

		// Print message to console
		System.out.println("Reading and modifying file 'exp2.genmodel'");

		// Read and manipulate the model
		readAndModifyModel();

		// Print message to console
		System.out.println("Model modified and saved as 'exp2-modified.genmodel'");
	}
	
	/*
	 * This function reads an existing model from the "robodsl" metamodel,
	 * alters it, and saves it as a new XMI file (with ".robodsl" extension).
	 * 
	 * It expects a file named "exp2.robodsl" in the root folder of the project,
	 * and it will create a file named "exp2-modified.robodsl" in the same place
	 */
	private static void readAndModifyModel() {
		// Obtain the instance of the package implementing the metamodel
		RobodslPackage pack = RobodslPackageImpl.eINSTANCE;
		// Obtain the factory (for creating instance of metaclasses)
		RobodslFactory factory = pack.getRobodslFactory();
		// Register the metamodel and the package with its implementation.
		// That is, add the URI of the metamodel in the global registry
		// and associate it with the package that implements it
		EPackage.Registry.INSTANCE.put(pack.getNsURI(), pack);
		
		// Create a ResourceSet, that is, a container for resources,
		// and specify that resources having the ".robodsl" extension
		// are serialized as XMI
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry()
			.getExtensionToFactoryMap()
			.put("robodsl", new XMIResourceFactoryImpl());

		/* UNCOMMENT HERE IF USING OCL CONSTRAINTS OR DERIVED FEATURES
		// Register the OCL delegates
		String oclDelegateURI = OCLDelegateDomain.OCL_DELEGATE_URI;
		EOperation.Internal.InvocationDelegate.Factory.Registry.INSTANCE.put(oclDelegateURI,
				new OCLInvocationDelegateFactory.Global());
		EStructuralFeature.Internal.SettingDelegate.Factory.Registry.INSTANCE.put(oclDelegateURI,
				new OCLSettingDelegateFactory.Global());
		EValidator.ValidationDelegate.Registry.INSTANCE.put(oclDelegateURI,
				new OCLValidationDelegateFactory.Global());
		   UNCOMMENT HERE IF USING OCL CONSTRAINTS OR DERIVED FEATURES */

		// Read a resource (XMI file)
		Resource res = rs.getResource(URI.createURI("exp2.robodsl"), true);
		
		// Get the Scene instance
		// (we know that it is the top-level element)
		Scene exp2 = (Scene)res.getContents().get(0);
		
		// 1) Change the name of the Scene to represent EXP3
		exp2.setName("EXP3");
		
		// 2) Find the two robots (instances of the Robot metaclass)
		Robot r2 = null;
		Robot r3 = null;
		for(Element el : exp2.getElements()) {
			if(el.getName().equals("R2")) {
				r2 = (Robot)el;
			}else if(el.getName().equals("R3")) {
				r3 = (Robot)el;
			}
		}
	
		// 3) Change the information of R3 (model and brand)
		r2.setBrand("SoftBank Robotics");
		r2.setModel("NAO");
		
		// 4) Change the behavior or R2
		// (create a new RunAway behavior and replace the old one)
		RunAway r3behavior = factory.createRunAway();
		r3behavior.setTarget(r2);
		r3.setBehavior(r3behavior);
		
		// Create a new resource (XMI file)
		Resource rMod = rs.createResource(URI.createURI("exp2-modified.robodsl"));
		
		// Add the scene to the new resource
		rMod.getContents().add(exp2);
		
		// Save the new resource to disk
		// (may throw a IOException)
		try {
			rMod.save(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
