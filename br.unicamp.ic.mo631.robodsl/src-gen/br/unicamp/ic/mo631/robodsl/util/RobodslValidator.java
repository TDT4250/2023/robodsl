/**
 */
package br.unicamp.ic.mo631.robodsl.util;

import br.unicamp.ic.mo631.robodsl.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage
 * @generated
 */
public class RobodslValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final RobodslValidator INSTANCE = new RobodslValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "br.unicamp.ic.mo631.robodsl";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RobodslValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return RobodslPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case RobodslPackage.ROBOT:
			return validateRobot((Robot) value, diagnostics, context);
		case RobodslPackage.BEHAVIOR:
			return validateBehavior((Behavior) value, diagnostics, context);
		case RobodslPackage.RANDOM:
			return validateRandom((Random) value, diagnostics, context);
		case RobodslPackage.GO_TO_POSITION:
			return validateGoToPosition((GoToPosition) value, diagnostics, context);
		case RobodslPackage.POSITION:
			return validatePosition((Position) value, diagnostics, context);
		case RobodslPackage.FOLLOW:
			return validateFollow((Follow) value, diagnostics, context);
		case RobodslPackage.RUN_AWAY:
			return validateRunAway((RunAway) value, diagnostics, context);
		case RobodslPackage.TARGETED_BEHAVIOR:
			return validateTargetedBehavior((TargetedBehavior) value, diagnostics, context);
		case RobodslPackage.SCENE:
			return validateScene((Scene) value, diagnostics, context);
		case RobodslPackage.ELEMENT:
			return validateElement((Element) value, diagnostics, context);
		case RobodslPackage.SIZE:
			return validateSize((Size) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRobot(Robot robot, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(robot, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(robot, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateElement_PositionAndSizeMustFitScene(robot, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBehavior(Behavior behavior, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(behavior, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRandom(Random random, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(random, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGoToPosition(GoToPosition goToPosition, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(goToPosition, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePosition(Position position, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(position, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validatePosition_PositionMustBeZeroOrGreater(position, diagnostics, context);
		if (result || diagnostics != null)
			result &= validatePosition_PositionMustBeWithinScene(position, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the PositionMustBeZeroOrGreater constraint of '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String POSITION__POSITION_MUST_BE_ZERO_OR_GREATER__EEXPRESSION = "x >= 0 and y >= 0";

	/**
	 * Validates the PositionMustBeZeroOrGreater constraint of '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePosition_PositionMustBeZeroOrGreater(Position position, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RobodslPackage.Literals.POSITION, position, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "PositionMustBeZeroOrGreater",
				POSITION__POSITION_MUST_BE_ZERO_OR_GREATER__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the PositionMustBeWithinScene constraint of '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String POSITION__POSITION_MUST_BE_WITHIN_SCENE__EEXPRESSION = "\n"
			+ "\t\t\tlet scene : Scene = \n" + "\t\t\t\tScene.allInstances()\n"
			+ "\t\t\t\t\t->select(s : Scene | s.oclContents->closure(oclContents())->includes(self))\n"
			+ "\t\t\t \t\t->asSequence()->first()\n" + "\t\t\tin\n" + "\t\t\tx < scene.size.x and y < scene.size.y";

	/**
	 * Validates the PositionMustBeWithinScene constraint of '<em>Position</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePosition_PositionMustBeWithinScene(Position position, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RobodslPackage.Literals.POSITION, position, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "PositionMustBeWithinScene",
				POSITION__POSITION_MUST_BE_WITHIN_SCENE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFollow(Follow follow, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(follow, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(follow, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateTargetedBehavior_CannotTargetItself(follow, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRunAway(RunAway runAway, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(runAway, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(runAway, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateTargetedBehavior_CannotTargetItself(runAway, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTargetedBehavior(TargetedBehavior targetedBehavior, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(targetedBehavior, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(targetedBehavior, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateTargetedBehavior_CannotTargetItself(targetedBehavior, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the CannotTargetItself constraint of '<em>Targeted Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TARGETED_BEHAVIOR__CANNOT_TARGET_ITSELF__EEXPRESSION = "target <> oclContainer";

	/**
	 * Validates the CannotTargetItself constraint of '<em>Targeted Behavior</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTargetedBehavior_CannotTargetItself(TargetedBehavior targetedBehavior,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate(RobodslPackage.Literals.TARGETED_BEHAVIOR, targetedBehavior, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "CannotTargetItself",
				TARGETED_BEHAVIOR__CANNOT_TARGET_ITSELF__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScene(Scene scene, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(scene, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElement(Element element, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(element, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(element, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateElement_PositionAndSizeMustFitScene(element, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the PositionAndSizeMustFitScene constraint of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ELEMENT__POSITION_AND_SIZE_MUST_FIT_SCENE__EEXPRESSION = "\n"
			+ "\t\t\tsize.oclIsUndefined() or\n"
			+ "\t\t\tlet minX : ecore::EDouble = startingPosition.x - size.x/2 in\n"
			+ "\t\t\t\tlet maxX : ecore::EDouble = startingPosition.x + size.x/2 in\n"
			+ "\t\t\t\t\tlet minY : ecore::EDouble = startingPosition.y - size.y/2 in\n"
			+ "\t\t\t\t\t\tlet maxY : ecore::EDouble = startingPosition.y + size.x/2 in\n"
			+ "\t\t\tminX >= 0 and maxX <= oclContainer.oclAsType(Scene).size.x-1 and \n"
			+ "\t\t\tminY >= 0 and maxY <= oclContainer.oclAsType(Scene).size.y-1 ";

	/**
	 * Validates the PositionAndSizeMustFitScene constraint of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateElement_PositionAndSizeMustFitScene(Element element, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RobodslPackage.Literals.ELEMENT, element, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "PositionAndSizeMustFitScene",
				ELEMENT__POSITION_AND_SIZE_MUST_FIT_SCENE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSize(Size size, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(size, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(size, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateSize_SizeMustBePositive(size, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the SizeMustBePositive constraint of '<em>Size</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SIZE__SIZE_MUST_BE_POSITIVE__EEXPRESSION = "x > 0 and y > 0";

	/**
	 * Validates the SizeMustBePositive constraint of '<em>Size</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSize_SizeMustBePositive(Size size, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RobodslPackage.Literals.SIZE, size, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "SizeMustBePositive",
				SIZE__SIZE_MUST_BE_POSITIVE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //RobodslValidator
