/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see br.unicamp.ic.mo631.robodsl.RobodslFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface RobodslPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "robodsl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://br.unicamp.ic.mo631/robodsl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "robodsl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RobodslPackage eINSTANCE = br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl.init();

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.ElementImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getElement()
	 * @generated
	 */
	int ELEMENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__SIZE = 1;

	/**
	 * The feature id for the '<em><b>Starting Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__STARTING_POSITION = 2;

	/**
	 * The feature id for the '<em><b>Closest Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__CLOSEST_ELEMENT = 3;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.RobotImpl <em>Robot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobotImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getRobot()
	 * @generated
	 */
	int ROBOT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__NAME = ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__SIZE = ELEMENT__SIZE;

	/**
	 * The feature id for the '<em><b>Starting Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__STARTING_POSITION = ELEMENT__STARTING_POSITION;

	/**
	 * The feature id for the '<em><b>Closest Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__CLOSEST_ELEMENT = ELEMENT__CLOSEST_ELEMENT;

	/**
	 * The feature id for the '<em><b>Brand</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__BRAND = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__MODEL = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Behavior</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__BEHAVIOR = ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Followed By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT__FOLLOWED_BY = ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Robot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Robot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_OPERATION_COUNT = ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.BehaviorImpl <em>Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.BehaviorImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getBehavior()
	 * @generated
	 */
	int BEHAVIOR = 1;

	/**
	 * The number of structural features of the '<em>Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.RandomImpl <em>Random</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.RandomImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getRandom()
	 * @generated
	 */
	int RANDOM = 2;

	/**
	 * The number of structural features of the '<em>Random</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FEATURE_COUNT = BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Random</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_OPERATION_COUNT = BEHAVIOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.GoToPositionImpl <em>Go To Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.GoToPositionImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getGoToPosition()
	 * @generated
	 */
	int GO_TO_POSITION = 3;

	/**
	 * The feature id for the '<em><b>Position</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_POSITION__POSITION = BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loop</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_POSITION__LOOP = BEHAVIOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Go To Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_POSITION_FEATURE_COUNT = BEHAVIOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Go To Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_POSITION_OPERATION_COUNT = BEHAVIOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.PositionImpl <em>Position</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.PositionImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getPosition()
	 * @generated
	 */
	int POSITION = 4;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION__Y = 1;

	/**
	 * The number of structural features of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Position</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.TargetedBehaviorImpl <em>Targeted Behavior</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.TargetedBehaviorImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getTargetedBehavior()
	 * @generated
	 */
	int TARGETED_BEHAVIOR = 7;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGETED_BEHAVIOR__TARGET = BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Targeted Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGETED_BEHAVIOR_FEATURE_COUNT = BEHAVIOR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Targeted Behavior</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGETED_BEHAVIOR_OPERATION_COUNT = BEHAVIOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.FollowImpl <em>Follow</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.FollowImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getFollow()
	 * @generated
	 */
	int FOLLOW = 5;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLLOW__TARGET = TARGETED_BEHAVIOR__TARGET;

	/**
	 * The number of structural features of the '<em>Follow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLLOW_FEATURE_COUNT = TARGETED_BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Follow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLLOW_OPERATION_COUNT = TARGETED_BEHAVIOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.RunAwayImpl <em>Run Away</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.RunAwayImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getRunAway()
	 * @generated
	 */
	int RUN_AWAY = 6;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUN_AWAY__TARGET = TARGETED_BEHAVIOR__TARGET;

	/**
	 * The number of structural features of the '<em>Run Away</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUN_AWAY_FEATURE_COUNT = TARGETED_BEHAVIOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Run Away</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUN_AWAY_OPERATION_COUNT = TARGETED_BEHAVIOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.SceneImpl <em>Scene</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.SceneImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getScene()
	 * @generated
	 */
	int SCENE = 8;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENE__ELEMENTS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENE__SIZE = 2;

	/**
	 * The feature id for the '<em><b>Area</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENE__AREA = 3;

	/**
	 * The number of structural features of the '<em>Scene</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Scene</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link br.unicamp.ic.mo631.robodsl.impl.SizeImpl <em>Size</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see br.unicamp.ic.mo631.robodsl.impl.SizeImpl
	 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getSize()
	 * @generated
	 */
	int SIZE = 10;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE__X = 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE__Y = 1;

	/**
	 * The number of structural features of the '<em>Size</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Size</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIZE_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Robot <em>Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Robot</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Robot
	 * @generated
	 */
	EClass getRobot();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Robot#getBrand <em>Brand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Brand</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Robot#getBrand()
	 * @see #getRobot()
	 * @generated
	 */
	EAttribute getRobot_Brand();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Robot#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Robot#getModel()
	 * @see #getRobot()
	 * @generated
	 */
	EAttribute getRobot_Model();

	/**
	 * Returns the meta object for the containment reference '{@link br.unicamp.ic.mo631.robodsl.Robot#getBehavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Behavior</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Robot#getBehavior()
	 * @see #getRobot()
	 * @generated
	 */
	EReference getRobot_Behavior();

	/**
	 * Returns the meta object for the reference list '{@link br.unicamp.ic.mo631.robodsl.Robot#getFollowedBy <em>Followed By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Followed By</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Robot#getFollowedBy()
	 * @see #getRobot()
	 * @generated
	 */
	EReference getRobot_FollowedBy();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Behavior <em>Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behavior</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Behavior
	 * @generated
	 */
	EClass getBehavior();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Random <em>Random</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Random</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Random
	 * @generated
	 */
	EClass getRandom();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.GoToPosition <em>Go To Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Go To Position</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.GoToPosition
	 * @generated
	 */
	EClass getGoToPosition();

	/**
	 * Returns the meta object for the containment reference list '{@link br.unicamp.ic.mo631.robodsl.GoToPosition#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Position</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.GoToPosition#getPosition()
	 * @see #getGoToPosition()
	 * @generated
	 */
	EReference getGoToPosition_Position();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.GoToPosition#isLoop <em>Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Loop</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.GoToPosition#isLoop()
	 * @see #getGoToPosition()
	 * @generated
	 */
	EAttribute getGoToPosition_Loop();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Position <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Position</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Position
	 * @generated
	 */
	EClass getPosition();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Position#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Position#getX()
	 * @see #getPosition()
	 * @generated
	 */
	EAttribute getPosition_X();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Position#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Position#getY()
	 * @see #getPosition()
	 * @generated
	 */
	EAttribute getPosition_Y();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Follow <em>Follow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Follow</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Follow
	 * @generated
	 */
	EClass getFollow();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.RunAway <em>Run Away</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Run Away</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.RunAway
	 * @generated
	 */
	EClass getRunAway();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.TargetedBehavior <em>Targeted Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Targeted Behavior</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.TargetedBehavior
	 * @generated
	 */
	EClass getTargetedBehavior();

	/**
	 * Returns the meta object for the reference '{@link br.unicamp.ic.mo631.robodsl.TargetedBehavior#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.TargetedBehavior#getTarget()
	 * @see #getTargetedBehavior()
	 * @generated
	 */
	EReference getTargetedBehavior_Target();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Scene <em>Scene</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scene</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Scene
	 * @generated
	 */
	EClass getScene();

	/**
	 * Returns the meta object for the containment reference list '{@link br.unicamp.ic.mo631.robodsl.Scene#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Scene#getElements()
	 * @see #getScene()
	 * @generated
	 */
	EReference getScene_Elements();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Scene#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Scene#getName()
	 * @see #getScene()
	 * @generated
	 */
	EAttribute getScene_Name();

	/**
	 * Returns the meta object for the containment reference '{@link br.unicamp.ic.mo631.robodsl.Scene#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Size</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Scene#getSize()
	 * @see #getScene()
	 * @generated
	 */
	EReference getScene_Size();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Scene#getArea <em>Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Area</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Scene#getArea()
	 * @see #getScene()
	 * @generated
	 */
	EAttribute getScene_Area();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Element#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Element#getName()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Name();

	/**
	 * Returns the meta object for the containment reference '{@link br.unicamp.ic.mo631.robodsl.Element#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Size</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Element#getSize()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Size();

	/**
	 * Returns the meta object for the containment reference '{@link br.unicamp.ic.mo631.robodsl.Element#getStartingPosition <em>Starting Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Starting Position</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Element#getStartingPosition()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_StartingPosition();

	/**
	 * Returns the meta object for the reference '{@link br.unicamp.ic.mo631.robodsl.Element#getClosestElement <em>Closest Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Closest Element</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Element#getClosestElement()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_ClosestElement();

	/**
	 * Returns the meta object for class '{@link br.unicamp.ic.mo631.robodsl.Size <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Size</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Size
	 * @generated
	 */
	EClass getSize();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Size#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Size#getX()
	 * @see #getSize()
	 * @generated
	 */
	EAttribute getSize_X();

	/**
	 * Returns the meta object for the attribute '{@link br.unicamp.ic.mo631.robodsl.Size#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see br.unicamp.ic.mo631.robodsl.Size#getY()
	 * @see #getSize()
	 * @generated
	 */
	EAttribute getSize_Y();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RobodslFactory getRobodslFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.RobotImpl <em>Robot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobotImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getRobot()
		 * @generated
		 */
		EClass ROBOT = eINSTANCE.getRobot();

		/**
		 * The meta object literal for the '<em><b>Brand</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROBOT__BRAND = eINSTANCE.getRobot_Brand();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROBOT__MODEL = eINSTANCE.getRobot_Model();

		/**
		 * The meta object literal for the '<em><b>Behavior</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT__BEHAVIOR = eINSTANCE.getRobot_Behavior();

		/**
		 * The meta object literal for the '<em><b>Followed By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT__FOLLOWED_BY = eINSTANCE.getRobot_FollowedBy();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.BehaviorImpl <em>Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.BehaviorImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getBehavior()
		 * @generated
		 */
		EClass BEHAVIOR = eINSTANCE.getBehavior();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.RandomImpl <em>Random</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.RandomImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getRandom()
		 * @generated
		 */
		EClass RANDOM = eINSTANCE.getRandom();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.GoToPositionImpl <em>Go To Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.GoToPositionImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getGoToPosition()
		 * @generated
		 */
		EClass GO_TO_POSITION = eINSTANCE.getGoToPosition();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GO_TO_POSITION__POSITION = eINSTANCE.getGoToPosition_Position();

		/**
		 * The meta object literal for the '<em><b>Loop</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GO_TO_POSITION__LOOP = eINSTANCE.getGoToPosition_Loop();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.PositionImpl <em>Position</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.PositionImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getPosition()
		 * @generated
		 */
		EClass POSITION = eINSTANCE.getPosition();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION__X = eINSTANCE.getPosition_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POSITION__Y = eINSTANCE.getPosition_Y();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.FollowImpl <em>Follow</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.FollowImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getFollow()
		 * @generated
		 */
		EClass FOLLOW = eINSTANCE.getFollow();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.RunAwayImpl <em>Run Away</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.RunAwayImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getRunAway()
		 * @generated
		 */
		EClass RUN_AWAY = eINSTANCE.getRunAway();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.TargetedBehaviorImpl <em>Targeted Behavior</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.TargetedBehaviorImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getTargetedBehavior()
		 * @generated
		 */
		EClass TARGETED_BEHAVIOR = eINSTANCE.getTargetedBehavior();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TARGETED_BEHAVIOR__TARGET = eINSTANCE.getTargetedBehavior_Target();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.SceneImpl <em>Scene</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.SceneImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getScene()
		 * @generated
		 */
		EClass SCENE = eINSTANCE.getScene();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENE__ELEMENTS = eINSTANCE.getScene_Elements();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENE__NAME = eINSTANCE.getScene_Name();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENE__SIZE = eINSTANCE.getScene_Size();

		/**
		 * The meta object literal for the '<em><b>Area</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENE__AREA = eINSTANCE.getScene_Area();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.ElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.ElementImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getElement()
		 * @generated
		 */
		EClass ELEMENT = eINSTANCE.getElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__SIZE = eINSTANCE.getElement_Size();

		/**
		 * The meta object literal for the '<em><b>Starting Position</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__STARTING_POSITION = eINSTANCE.getElement_StartingPosition();

		/**
		 * The meta object literal for the '<em><b>Closest Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__CLOSEST_ELEMENT = eINSTANCE.getElement_ClosestElement();

		/**
		 * The meta object literal for the '{@link br.unicamp.ic.mo631.robodsl.impl.SizeImpl <em>Size</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see br.unicamp.ic.mo631.robodsl.impl.SizeImpl
		 * @see br.unicamp.ic.mo631.robodsl.impl.RobodslPackageImpl#getSize()
		 * @generated
		 */
		EClass SIZE = eINSTANCE.getSize();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIZE__X = eINSTANCE.getSize_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIZE__Y = eINSTANCE.getSize_Y();

	}

} //RobodslPackage
