/**
 */
package br.unicamp.ic.mo631.robodsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Position</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Position#getX <em>X</em>}</li>
 *   <li>{@link br.unicamp.ic.mo631.robodsl.Position#getY <em>Y</em>}</li>
 * </ul>
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getPosition()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='PositionMustBeZeroOrGreater PositionMustBeWithinScene'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot PositionMustBeZeroOrGreater='x &gt;= 0 and y &gt;= 0' PositionMustBeWithinScene='\n\t\t\tlet scene : Scene = \n\t\t\t\tScene.allInstances()\n\t\t\t\t\t-&gt;select(s : Scene | s.oclContents-&gt;closure(oclContents())-&gt;includes(self))\n\t\t\t \t\t-&gt;asSequence()-&gt;first()\n\t\t\tin\n\t\t\tx &lt; scene.size.x and y &lt; scene.size.y'"
 * @generated
 */
public interface Position extends EObject {
	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(double)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getPosition_X()
	 * @model required="true"
	 * @generated
	 */
	double getX();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Position#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(double value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(double)
	 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getPosition_Y()
	 * @model required="true"
	 * @generated
	 */
	double getY();

	/**
	 * Sets the value of the '{@link br.unicamp.ic.mo631.robodsl.Position#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(double value);

} // Position
