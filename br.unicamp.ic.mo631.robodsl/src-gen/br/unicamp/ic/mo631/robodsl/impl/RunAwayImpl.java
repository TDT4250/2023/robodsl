/**
 */
package br.unicamp.ic.mo631.robodsl.impl;

import br.unicamp.ic.mo631.robodsl.RobodslPackage;
import br.unicamp.ic.mo631.robodsl.RunAway;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Run Away</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RunAwayImpl extends TargetedBehaviorImpl implements RunAway {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RunAwayImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RobodslPackage.Literals.RUN_AWAY;
	}

} //RunAwayImpl
