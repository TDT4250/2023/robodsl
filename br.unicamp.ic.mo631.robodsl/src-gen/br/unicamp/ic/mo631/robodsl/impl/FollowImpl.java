/**
 */
package br.unicamp.ic.mo631.robodsl.impl;

import br.unicamp.ic.mo631.robodsl.Follow;
import br.unicamp.ic.mo631.robodsl.RobodslPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Follow</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FollowImpl extends TargetedBehaviorImpl implements Follow {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FollowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RobodslPackage.Literals.FOLLOW;
	}

} //FollowImpl
