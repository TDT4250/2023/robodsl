/**
 */
package br.unicamp.ic.mo631.robodsl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Run Away</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.unicamp.ic.mo631.robodsl.RobodslPackage#getRunAway()
 * @model
 * @generated
 */
public interface RunAway extends TargetedBehavior {
} // RunAway
